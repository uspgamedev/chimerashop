extends Node

# Husband Stuff
enum HUSBAND_ATTR {STRENGTH, CHARM, BEAUTY}
enum HUSBAND_PART {HEAD, BODY, EXTRA}

var husbands_created : Array = [];

var available_heads : Array = [];
var available_bodies : Array = [];
var available_extras : Array = [];

func add_part(part : HusbandPart) -> void:
	match part.part_type:
		GlobalEnums.HUSBAND_PART.HEAD:
			available_heads.append(part);
		GlobalEnums.HUSBAND_PART.BODY:
			available_bodies.append(part);
		GlobalEnums.HUSBAND_PART.EXTRA:
			available_extras.append(part);
	
