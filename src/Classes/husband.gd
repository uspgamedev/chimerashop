extends Area2D
class_name Husband


var husband_name : String;
var production_price : int = 0;
var sell_price : int = 0;


var _attributes : Dictionary = {
	GlobalEnums.HUSBAND_ATTR.STRENGTH : 0,
	GlobalEnums.HUSBAND_ATTR.CHARM : 0,
	GlobalEnums.HUSBAND_ATTR.BEAUTY : 0,
};

var _parts : Dictionary = {
	GlobalEnums.HUSBAND_PART.HEAD : null,
	GlobalEnums.HUSBAND_PART.BODY : null,
	GlobalEnums.HUSBAND_PART.EXTRA : null,
};

	
func get_strength() -> int:
	return _attributes[GlobalEnums.HUSBAND_ATTR.STRENGTH];

func set_strength(value : int) -> void:
	_attributes[GlobalEnums.HUSBAND_ATTR.STRENGTH] = value;


func get_charm() -> int:
	return _attributes[GlobalEnums.HUSBAND_ATTR.CHARM];

func set_charm(value : int) -> void:
	_attributes[GlobalEnums.HUSBAND_ATTR.CHARM] = value;


func get_beauty() -> int:
	return _attributes[GlobalEnums.HUSBAND_ATTR.BEAUTY];

func set_beauty(value : int) -> void:
	_attributes[GlobalEnums.HUSBAND_ATTR.BEAUTY] = value;


func get_head() -> HusbandPart:
	return _parts[GlobalEnums.HUSBAND_PART.HEAD];

func set_head(value : HusbandPart) -> void:
	assert(value.part_type == GlobalEnums.HUSBAND_PART.HEAD)
	_parts[GlobalEnums.HUSBAND_PART.HEAD] = value;


func get_body() -> HusbandPart:
	return _parts[GlobalEnums.HUSBAND_PART.BODY];
	
func set_body(value : HusbandPart) -> void:
	assert(value.part_type == GlobalEnums.HUSBAND_PART.BODY)
	_parts[GlobalEnums.HUSBAND_PART.BODY] = value;


func get_extra() -> HusbandPart:
	return _parts[GlobalEnums.HUSBAND_PART.EXTRA];
	
func set_extra(value : HusbandPart) -> void:
	assert(value.part_type == GlobalEnums.HUSBAND_PART.EXTRA)
	_parts[GlobalEnums.HUSBAND_PART.EXTRA] = value;

func clone() -> Husband:
	var clone = self.duplicate();
	clone.husband_name = self.husband_name;
	clone.production_price = self.production_price;
	clone.sell_price = self.sell_price;
	clone.set_strength(self.get_strength());
	clone.set_charm(self.get_charm());
	clone.set_beauty(self.get_beauty());
	return clone;
