extends Control

export(String) var game_scene_name : String = \
		"res://game_scenes/game_scene.tscn";

func _on_start_pressed():
	get_tree().change_scene(game_scene_name);

func _on_quit_pressed():
	get_tree().quit();

func _on_credits_pressed():
	$credits_menu.show();

func _on_back_credits_pressed():
	$credits_menu.hide();
