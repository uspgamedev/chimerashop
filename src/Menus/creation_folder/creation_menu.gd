extends Control


signal husband_created(husband)
signal husband_canceled()


func show_menu():
	$project_name/value.text = "";
	for child in $part_selectors.get_children():
		child.part_index = 0;
		child.show_part();

func save_husband():
	var husband = Husband.new();
	husband.husband_name = $project_name/value.text;

	var head = $part_selectors/head_selector.current_part;
	var body = $part_selectors/body_selector.current_part;
	var extra = $part_selectors/extra_selector.current_part;

	husband.set_head(head);
	husband.set_body(body);
	husband.set_extra(extra);

	husband.set_strength(
		head.get_strength() + body.get_strength() + extra.get_strength()
	);
	husband.set_charm(
		head.get_charm() + body.get_charm() +  extra.get_charm()
	);
	husband.set_beauty(
		head.get_beauty() + body.get_beauty() + extra.get_beauty()
	);
	
	husband.production_price = head.price + body.price + extra.price;

	emit_signal("husband_created", husband);

func cancel():
	emit_signal("husband_canceled");
