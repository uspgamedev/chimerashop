extends Control

export(GlobalEnums.HUSBAND_PART) var part_type;
export(int) var part_index : int = 0;

var available_parts : Array = []; # array of parts to select
var current_part;

func _ready():
	match part_type:
		GlobalEnums.HUSBAND_PART.HEAD:
			available_parts = GlobalEnums.available_heads;
		GlobalEnums.HUSBAND_PART.BODY:
			available_parts = GlobalEnums.available_bodies;
		GlobalEnums.HUSBAND_PART.EXTRA:
			available_parts = GlobalEnums.available_extras;

func show_part():
	var husband_part = available_parts[part_index];
	$info/name.text = str(husband_part.part_name);
	$part_image.texture = husband_part.part_image;
	$info/price_info/value.text = str(husband_part.price);
	$info/strength_info/value.text = str(husband_part.get_strength());
	$info/beauty_info/value.text = str(husband_part.get_beauty());
	$info/charm_info/value.text = str(husband_part.get_charm());

	current_part = husband_part;

func _on_next_pressed():
	if part_index < available_parts.size() - 1:
		part_index += 1;
	else:
		part_index = 0;
	show_part();

func _on_previous_pressed():
	if part_index > 0:
		part_index -= 1;
	else:
		part_index = available_parts.size() - 1;
	show_part();
