extends Control

signal husband_selected(husband)
signal husband_cancel()

var _selected_husband : Husband = null;

func _ready():
	$Menu/husband_select/Husband.get_popup().connect("id_pressed", self, "_project_selected");

func open_menu():
	$Menu/husband_select/Husband.get_popup().clear()
	for i in range(GlobalEnums.husbands_created.size()):
		var husband : Husband = GlobalEnums.husbands_created[i];
		$Menu/husband_select/Husband.get_popup().add_item(husband.husband_name, i);

	_selected_husband = null
	$Menu/Name/Value.text = ""
	$Menu/Strengh/Value.text = "0"
	$Menu/Charm/Value.text = "0"
	$Menu/Beauty/Value.text = "0"
	$Menu/ProductionPrice/Value.text = "0"
	$Menu/SellPrice/Value.value = 0
	$Menu/Image/Head.texture = null;
	$Menu/Image/Body.texture = null;
	$Menu/Image/Extra.texture = null;

	self.show()

func _project_selected(index : int):
	_selected_husband = GlobalEnums.husbands_created[index];
	$Menu/Name/Value.text = _selected_husband.husband_name;
	$Menu/Strengh/Value.text = str(_selected_husband.get_strength())
	$Menu/Charm/Value.text = str(_selected_husband.get_charm())
	$Menu/Beauty/Value.text = str(_selected_husband.get_beauty())
	$Menu/Image/Head.texture = _selected_husband.get_head().part_image;
	$Menu/Image/Body.texture = _selected_husband.get_body().part_image;
	$Menu/Image/Extra.texture = _selected_husband.get_extra().part_image;
	$Menu/ProductionPrice/Value.text = str(_selected_husband.production_price)
	$Menu/SellPrice/Value.value = _selected_husband.production_price
	_selected_husband.sell_price = _selected_husband.production_price
	
	self.emit_signal("husband_selected", _selected_husband);

func update_money(money : int):
	if _selected_husband != null and \
			_selected_husband.production_price > money:
		$Menu/ProductionPrice/warning.show();
	else:
		$Menu/ProductionPrice/warning.hide();

func show_cant_produce_husband():
	$Menu/ProductionPrice/warning.show();

func _on_sell_price_changed(value : int):
	if _selected_husband == null:
		return;
	_selected_husband.sell_price = int(value);
	self.emit_signal("husband_selected", _selected_husband);

func _on_Cancel_pressed():
	_selected_husband = null;
	$Menu/ProductionPrice/warning.hide();
	self.emit_signal("husband_cancel");
