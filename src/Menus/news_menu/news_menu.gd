extends Panel

export(String) var trend_events_filename : String = "res://data/trend_events.json";
var _strength_events : Array = [];
var _charm_events : Array = [];
var _beauty_events : Array = [];

# Called when the node enters the scene tree for the first time.
func _ready():
	var events_file = File.new();
	events_file.open(trend_events_filename, File.READ);
	var events_dict = parse_json(events_file.get_as_text());
	
	_strength_events = events_dict["strength"];
	_charm_events = events_dict["charm"];
	_beauty_events = events_dict["beauty"];
	
	events_file.close();

func show_trend_event(type_of_event : int):
	var trend_text = "";
	match type_of_event:
		GlobalEnums.HUSBAND_ATTR.STRENGTH:
			trend_text = _strength_events[randi()%_strength_events.size()];
		GlobalEnums.HUSBAND_ATTR.CHARM:
			trend_text = _charm_events[randi()%_charm_events.size()];
		GlobalEnums.HUSBAND_ATTR.BEAUTY:
			trend_text = _beauty_events[randi()%_beauty_events.size()];
	
	$news_info/text.text = trend_text;

func show_retirement():
	$news_info/text.text = \
	"Local husband store owner gets retired, but is still in business";
