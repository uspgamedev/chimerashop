extends Control

signal purchase_cancel();
signal buy_random_part();
signal retire();
signal back_main_menu();

var _unlock_cost : int = 0;
var _retirement_cost : int = 0;
var _is_sold_out : bool = false;
var _main_menu_scene_name : String = "";

func set_price(price : int):
	_unlock_cost = price;
	$purchase_interface/price_info/value.text = str(price);

func set_retirement_cost(cost : int):
	_retirement_cost = cost;
	$retire_interface/price_info/value.text = str(cost);

func show_menu():
	$part_display/part_image.texture = null;
	$part_display/info/name_info/value.text = "";
	$part_display/info/strength_info/value.text = "";
	$part_display/info/charm_info/value.text = "";
	$part_display/info/beauty_info/value.text = "";

func show_part(husband_part : HusbandPart):
	$part_display/part_image.texture = husband_part.part_image;
	$part_display/info/name_info/value.text = husband_part.part_name;
	$part_display/info/strength_info/value.text = \
			str(husband_part.get_strength());
	$part_display/info/charm_info/value.text = \
		str(husband_part.get_charm());
	$part_display/info/beauty_info/value.text = \
			str(husband_part.get_beauty());
	
	$part_display/animation_player.play("unlock_part");

func update_money(money : int):
	if _is_sold_out:
		return;
	
	if _unlock_cost > money:
		show_cant_buy_part();
	else:
		$purchase_interface/warning.hide();
	
	if _retirement_cost > money:
		show_cant_retire();
	else:
		$retire_interface/warning.hide();

func show_cant_buy_part():
	$purchase_interface/warning.text = "Not enough money";
	$purchase_interface/warning.show();

func show_sold_out():
	$purchase_interface/warning.text = "Sold Out!";
	$purchase_interface/warning.show();
	_is_sold_out = true;
	$retire_interface.show();

func show_retired():
	$win_screen.show();

func show_cant_retire():
	$retire_interface/warning.show();

func _on_back_pressed():
	if !(_is_sold_out):
		$purchase_interface/warning.hide();
	$part_display/animation_player.play("RESET");
	self.emit_signal("purchase_cancel");

func _on_buy_button_pressed():
	self.emit_signal("buy_random_part");

func _on_retire_button_pressed():
	self.emit_signal("retire");

func _on_menu_button_pressed():
	emit_signal("back_main_menu");

func _on_back_to_game_button_pressed():
	$win_screen.hide();

func disable_retire_button():
	$retire_interface/retire_button.disabled = true;
