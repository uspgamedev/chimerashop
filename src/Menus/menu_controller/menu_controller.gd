extends Control

enum {NONE, CREATE, SELECT, UNLOCK};
export(NodePath) var store_manager_path : NodePath;
export(String) var main_menu_scene_name : String = \
		"res://menus/main_menu/main_menu.tscn";
 
var _open_menu : int = 0;
var _store_manager : Node2D;

func _ready():
	_store_manager = get_node(store_manager_path);
	update_money(_store_manager.money);
	$unlock_parts_menu.set_retirement_cost(_store_manager.retirement_cost);

func update_money(money : int):
	$money/money_info/value.text = str(money);
	$unlock_parts_menu.update_money(money);
	$select_husband_menu.update_money(money);

func add_log(text : String):
	$log_menu/log_panel/scroll_container/margin/log.text += text + "\n";
	$log_menu/log_panel/scroll_container.scroll_vertical = \
			$log_menu/log_panel/scroll_container.get_v_scrollbar().max_value

func show_new_trend(type_of_trend : int):
	$news_menu.show_trend_event(type_of_trend);

func show_unlock_cost(cost : int):
	$unlock_parts_menu.set_price(cost);

func show_unlocked_part(husband_part : HusbandPart):
	if _open_menu == UNLOCK:
		$unlock_parts_menu.show_part(husband_part);

func show_retirement_cost(cost : int):
	$unlock_parts_menu.set_retirement_cost(cost);

func show_retired():
	if _open_menu == UNLOCK:
		$unlock_parts_menu.show_retired();
		$news_menu.show_retirement();

func show_parts_sold_out():
	if _open_menu == UNLOCK:
		$unlock_parts_menu.show_sold_out();

func show_cant_buy_part():
	if _open_menu == UNLOCK:
		$unlock_parts_menu.show_cant_buy_part();

func show_cant_produce_husband():
	if _open_menu == SELECT:
		$select_husband_menu.show_cant_produce_husband();


# warning-ignore:unused_argument
func _on_CreateHusband_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
			and event.button_index == 1 \
			and event.pressed:

		match _open_menu:
			NONE:
				_open_menu = CREATE;
				$creation_menu.show_menu();
				$creation_menu.show();
				$log_menu.hide();
			SELECT:
				_on_SelectHusbandMenu_husband_cancel();
				_open_menu = CREATE;
				$creation_menu.show_menu();
				$creation_menu.show();
				$log_menu.hide();

# warning-ignore:unused_argument
func _on_SelectHusband_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
			and event.button_index == 1 \
			and event.pressed:

		match _open_menu:
			NONE:
				_open_menu = SELECT;
				$select_husband_menu.open_menu();
				$select_husband_menu.show();
				$log_menu.hide();
			CREATE:
				_on_SelectHusbandMenu_husband_cancel();
				_open_menu = SELECT;
				$select_husband_menu.open_menu();
				$select_husband_menu.show();
				$log_menu.hide();

# warning-ignore:unused_argument
func _on_UnlockParts_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
			and event.button_index == 1 \
			and event.pressed:

		match _open_menu:
			NONE:
				_open_menu = UNLOCK;
				$unlock_parts_menu.show_menu();
				$unlock_parts_menu.show();
			SELECT:
				_on_SelectHusbandMenu_husband_cancel();
				_open_menu = UNLOCK;
				$unlock_parts_menu.show_menu();
				$unlock_parts_menu.show();


func _on_unlock_parts_buy_random_part():
	_store_manager.buy_random_part();

func _on_unlock_parts_purchase_cancel():
	$unlock_parts_menu.hide();
	_open_menu = NONE;

func _on_retire():
	_store_manager.retire();
	$unlock_parts_menu.disable_retire_button();

func show_cant_retire():
	if _open_menu == UNLOCK:
		$unlock_parts_menu.show_cant_retire();

func _on_creation_menu_husband_created(husband : Husband):
	GlobalEnums.husbands_created.append(husband);
	$creation_menu.hide();
	$log_menu.show();
	_open_menu = NONE;

func _on_creation_menu_husband_canceled():
	$creation_menu.hide();
	$log_menu.show();
	_open_menu = NONE;


func _on_SelectHusbandMenu_husband_selected(husband):
	#print("Selected");
	if husband == null:
		_on_SelectHusbandMenu_husband_cancel();
		return
	_store_manager.set_holding_husband(husband);

func _on_SelectHusbandMenu_husband_cancel():
	_store_manager.set_holding_husband(null);
	$select_husband_menu.hide();
	$log_menu.show();
	_open_menu = NONE;


func _on_menu_pressed():
	GlobalEnums.available_heads = []
	GlobalEnums.available_bodies = [];
	GlobalEnums.available_extras = [];
	GlobalEnums.husbands_created = [];
	get_tree().change_scene(main_menu_scene_name);


