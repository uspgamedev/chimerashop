extends Node2D

export(PackedScene) var gorilla_body;
export(PackedScene) var eagle_head;
export(PackedScene) var rat_extra;

export(PackedScene) var fox_body;
export(PackedScene) var wolf_head;
export(PackedScene) var monkey_extra;

export(PackedScene) var croc_body;
export(PackedScene) var frog_head;
export(PackedScene) var panda_extra;

func _ready():
	var gorilla_body_instance = gorilla_body.instance();
	GlobalEnums.available_bodies.append(gorilla_body_instance);
	var eagle_head_instance = eagle_head.instance();
	GlobalEnums.available_heads.append(eagle_head_instance);
	var rat_extra_instance = rat_extra.instance();
	GlobalEnums.available_extras.append(rat_extra_instance);
	
	var fox_body_instance = fox_body.instance();
	GlobalEnums.available_bodies.append(fox_body_instance);
	var wolf_head_instance = wolf_head.instance();
	GlobalEnums.available_heads.append(wolf_head_instance);
	var monkey_extra_instance = monkey_extra.instance();
	GlobalEnums.available_extras.append(monkey_extra_instance);
	
	var croc_body_instance = croc_body.instance();
	GlobalEnums.available_bodies.append(croc_body_instance);
	var frog_head_instance = frog_head.instance();
	GlobalEnums.available_heads.append(frog_head_instance);
	var panda_extra_instance = panda_extra.instance();
	GlobalEnums.available_extras.append(panda_extra_instance);
	
	$creation_menu.show_menu();
