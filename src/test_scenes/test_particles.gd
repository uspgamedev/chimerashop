extends Node2D

export(PackedScene) var particles : PackedScene
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var particles_instance = particles.instance();
	self.call_deferred("add_child", particles_instance);
	#print(particles_instance);


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
