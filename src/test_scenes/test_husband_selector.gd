extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var husband = Husband.new();
	husband.husband_name = "Teste 1";
	husband.set_strength(1);
	husband.set_charm(2);
	husband.set_beauty(3);
	husband.production_price = 10;
	GlobalEnums.husbands_created.append(husband);

	husband = Husband.new();
	husband.husband_name = "Teste 2";
	husband.set_strength(3);
	husband.set_charm(3);
	husband.set_beauty(5);
	husband.production_price = 100;
	GlobalEnums.husbands_created.append(husband);

	$SelectHusbandMenu.open_menu();

func _on_SelectHusbandMenu_husband_selected(husband):
	$HusbandStandManager._holding_husband = husband;

func _on_SelectHusbandMenu_husband_cancel():
	$HusbandStandManager._holding_husband = null;
