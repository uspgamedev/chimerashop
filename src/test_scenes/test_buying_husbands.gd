extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var husband = Husband.new();
	husband.husband_name = "Teste 1";
	husband.set_strength(1);
	husband.set_charm(2);
	husband.set_beauty(3);
	husband.production_price = 10;
	husband.sell_price = 12;
	GlobalEnums.husbands_created.append(husband);
	
	$HusbandStandManager.set_husband(husband);
