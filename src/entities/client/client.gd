extends Node
class_name Client


signal walk_around(client) # Called when the client wants to walk around the store
signal leave_store(client) # Called when the client wants to leave the store
signal left(client) # Called when the customer left the map


export(NodePath) var walk_timer_path : NodePath;


var client_name : String;
var speed : float = 100.0;
var has_bought_husband : bool = false;


var _desired_attr : Dictionary = {
	GlobalEnums.HUSBAND_ATTR.STRENGTH : 0,
	GlobalEnums.HUSBAND_ATTR.CHARM : 0,
	GlobalEnums.HUSBAND_ATTR.BEAUTY : 0,
}
var _is_vip : bool = false;
var _tolerence : float = 1.1;
var _tolerence_weight : float = 0.05;
var _is_walking : bool = true;
var _is_leaving : bool = false;
var _walk_path : PoolVector2Array = [];
var _walk_timer : Timer = null;
var _walk_counter : int = 0; # how many times the client has walked


func setup_client(current_trend : Dictionary, name : String):
	client_name = name;
	_is_vip = (randi()%10 == 1); # 10% change of being VIP
	if _is_vip: # VIP's demand more but also pay more
		$vip_crown.show();
		$vip_particles.emitting = true;
		client_name += " (VIP)";
		# Base attributes in [3, 5]
		_desired_attr[GlobalEnums.HUSBAND_ATTR.STRENGTH] = 3 + randi()%3 + \
				current_trend[GlobalEnums.HUSBAND_ATTR.STRENGTH];
		
		_desired_attr[GlobalEnums.HUSBAND_ATTR.CHARM] = 3 + randi()%3 + \
				current_trend[GlobalEnums.HUSBAND_ATTR.CHARM];
		
		_desired_attr[GlobalEnums.HUSBAND_ATTR.BEAUTY] = 3 + randi()%3 + \
				current_trend[GlobalEnums.HUSBAND_ATTR.BEAUTY];
		
		_tolerence = 1.8 + (randi()%3) * 0.1; # varies from 1.8 to 2.1
		_tolerence_weight = 0.025;
	else:
		# Base attributes in [0, 3]
		_desired_attr[GlobalEnums.HUSBAND_ATTR.STRENGTH] = randi()%4 + \
				current_trend[GlobalEnums.HUSBAND_ATTR.STRENGTH];
		
		_desired_attr[GlobalEnums.HUSBAND_ATTR.CHARM] = randi()%4 + \
				current_trend[GlobalEnums.HUSBAND_ATTR.CHARM];
		
		_desired_attr[GlobalEnums.HUSBAND_ATTR.BEAUTY] = randi()%4 + \
				current_trend[GlobalEnums.HUSBAND_ATTR.BEAUTY];
		
		_tolerence = 1.4 + (randi()%3) * 0.1; # varies from 1.4 to 1.6
		_tolerence_weight = 0.05;

	self.modulate = Color(rand_range(0, 1), rand_range(0, 1), rand_range(0, 1))
	_walk_timer = get_node(walk_timer_path);

func _process(delta):
	if _is_walking:
		var walk_distance = speed * delta
		_move_along_path(walk_distance)

func buy_husband(husbandStand : HusbandStand) -> void:
	husbandStand.buy_husband(self);
	_is_leaving = true;
	has_bought_husband = true;
	self.emit_signal("leave_store", self);

# Checks if this client wants to buy the husband provided
func analise_husband(husbandStand : HusbandStand) -> void:
	#print(name + "(" + str(_is_vip) + "): " + str(_desired_attr));
	
	var chance_ignore_tolerence = lerp(0, (_tolerence - 1), _tolerence_weight);
	#print("Chance ignore tol: " + str(chance_ignore_tolerence));
	if randf() < chance_ignore_tolerence:
		#print("Ignore tolerence");
		buy_husband(husbandStand);
		return;
	
	var husband = husbandStand.husband_set;
	var tolerence_temp = _tolerence;
	
	var attribute_diffs = Array();
	attribute_diffs.append(husband.get_strength() - self.get_strength());
	attribute_diffs.append(husband.get_charm() - self.get_charm());
	attribute_diffs.append(husband.get_beauty() - self.get_beauty());
	for attribute_diff in attribute_diffs:
		if attribute_diff < 0:
			# if an attibute is not satisfied, remove 0.1 from tolerence
			tolerence_temp -= 0.1;
		elif attribute_diff > 0:
			# if an attribute is bigger, add 0.1 to tolerence
			tolerence_temp += 0.1 ;

	#print("Original tolerence: " + str(_tolerence));
	#print("Purchase tolerence: " + str(tolerence_temp));

	if husband.sell_price <= husband.production_price * tolerence_temp:
		buy_husband(husbandStand);

func set_walk_path(path : PoolVector2Array):
	_walk_path = path
	_is_walking = true

func get_strength() -> int:
	return _desired_attr[GlobalEnums.HUSBAND_ATTR.STRENGTH];

func set_strength(value : int) -> void:
	_desired_attr[GlobalEnums.HUSBAND_ATTR.STRENGTH] = value;


func get_charm() -> int:
	return _desired_attr[GlobalEnums.HUSBAND_ATTR.CHARM];

func set_charm(value : int) -> void:
	_desired_attr[GlobalEnums.HUSBAND_ATTR.CHARM] = value;


func get_beauty() -> int:
	return _desired_attr[GlobalEnums.HUSBAND_ATTR.BEAUTY];

func set_beauty(value : int) -> void:
	_desired_attr[GlobalEnums.HUSBAND_ATTR.BEAUTY] = value;


func _on_husband_found(husbandStand : Area2D) -> void:
	if not _is_leaving \
			and husbandStand is HusbandStand \
			and husbandStand.husband_set != null:
		analise_husband(husbandStand);


func _on_walk_timer_timeout() -> void:
	if _is_leaving:
		return
	
	if rand_range(0, 1) > 0.8 - (_walk_counter * 0.05):
		_is_leaving = true;
		self.emit_signal("leave_store", self);
	else:
		_walk_counter += 1;
		self.emit_signal("walk_around", self);

# Taken from Godot's Navigation2D demo
func _move_along_path(distance):
	var last_point = self.position;
	while _walk_path.size():
		var distance_between_points = last_point.distance_to(_walk_path[0]);
		# The position to move to falls between two points.
		if distance <= distance_between_points:
			self.position = last_point.linear_interpolate(
				_walk_path[0], 
				distance / distance_between_points
			);
			return;
		# The position is past the end of the segment.
		distance -= distance_between_points;
		last_point = _walk_path[0];
		_walk_path.remove(0);
	# The character reached the end of the _walk_path.
	self.position = last_point;
	_is_walking = false;

	if not _is_leaving:
		_walk_timer.start(rand_range(1.0, 3.0));

	else:
		self.emit_signal("left", self);
		set_process(false);
