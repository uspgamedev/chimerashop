extends Node2D

signal new_trend(type_of_trend);
signal client_left_log(log_text);

const MAX_CLIENTS = 10;

export(int) var trend_wait_time : int = 60;
export(String) var client_names_file : String = "res://data/client_names.txt";
export(String) var client_surnames_file : String = "res://data/client_surnames.txt";
export(PackedScene) var client_scene : PackedScene;
export(NodePath) var store_path : NodePath;

var _store : Store;
var _clients : Array = [];
var _client_names : Array = [];
var _client_surnames : Array = [];
var _current_trend : Dictionary = {
	GlobalEnums.HUSBAND_ATTR.STRENGTH : 0,
	GlobalEnums.HUSBAND_ATTR.CHARM : 0,
	GlobalEnums.HUSBAND_ATTR.BEAUTY : 0,
};

func _ready():
	_store = get_node(store_path);
	$trend_timer.wait_time = trend_wait_time;
	$trend_timer.start();
	
	var name_file = File.new();
	name_file.open(client_names_file, File.READ);
	while not name_file.eof_reached():
		_client_names.append(name_file.get_line());
	name_file.close();
	
	var surname_file = File.new();
	surname_file.open(client_surnames_file, File.READ);
	while not surname_file.eof_reached():
		_client_surnames.append(surname_file.get_line());
	surname_file.close();

func _on_trend_timer_timeout():
	var attribute = randi()%3; # gets a random attribute
	var value = 1 + randi()%2; # gets a value in [1,2]
	match attribute:
		GlobalEnums.HUSBAND_ATTR.STRENGTH:
			_current_trend[GlobalEnums.HUSBAND_ATTR.STRENGTH] = value;
			_current_trend[GlobalEnums.HUSBAND_ATTR.CHARM] = 0;
			_current_trend[GlobalEnums.HUSBAND_ATTR.BEAUTY] = 0;
		
		GlobalEnums.HUSBAND_ATTR.CHARM:
			_current_trend[GlobalEnums.HUSBAND_ATTR.STRENGTH] = 0;
			_current_trend[GlobalEnums.HUSBAND_ATTR.CHARM] = value;
			_current_trend[GlobalEnums.HUSBAND_ATTR.BEAUTY] = 0;
		
		GlobalEnums.HUSBAND_ATTR.BEAUTY:
			_current_trend[GlobalEnums.HUSBAND_ATTR.STRENGTH] = 0;
			_current_trend[GlobalEnums.HUSBAND_ATTR.CHARM] = 0;
			_current_trend[GlobalEnums.HUSBAND_ATTR.BEAUTY] = value;
	
	self.emit_signal("new_trend", attribute);

func on_retire():
	_current_trend = {
	GlobalEnums.HUSBAND_ATTR.STRENGTH : 0,
	GlobalEnums.HUSBAND_ATTR.CHARM : 0,
	GlobalEnums.HUSBAND_ATTR.BEAUTY : 0,
	};
	$trend_timer.start();

func _on_SpawnTimer_timeout():
	if(_clients.size() < MAX_CLIENTS):
		_spawn_client()

func _spawn_client():
	var new_client : Client = client_scene.instance();
	new_client.position = _store.get_entry_point();
	# warning-ignore:return_value_discarded
	new_client.connect("walk_around", self, "_set_client_store_path")
	# warning-ignore:return_value_discarded
	new_client.connect("leave_store", self, "_set_client_exit_path")
	# warning-ignore:return_value_discarded
	new_client.connect("left", self, "_destroy_client")
	
	# Generate a random name
	var client_name = _client_names[randi()%_client_names.size()] + " " + \
			_client_surnames[randi()%_client_surnames.size()];
	
	new_client.setup_client(_current_trend, client_name);
	_clients.append(new_client)
	self.call_deferred("add_child", new_client);
	yield(get_tree(), "idle_frame");

func _set_client_store_path(client : Client):
	client.set_walk_path(_store.get_store_path(client.position))

func _set_client_exit_path(client : Client):
	if !(client.has_bought_husband):
		var log_text = client.client_name + " didn't find the husband they wanted";
		self.emit_signal("client_left_log", log_text);
	
	client.set_walk_path(_store.get_exit_path(client.position))

func _destroy_client(client : Client):
	_clients.remove(_clients.find(client));
	client.call_deferred("free")
