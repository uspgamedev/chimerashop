extends Node2D
class_name Store

signal no_parts_to_buy();
signal cant_buy_part();
signal cant_produce();
signal unlocked_part(husband_part);
signal money_changed(money);
signal set_retirement_cost(cost);
signal retired();
signal cant_retire();

export(NodePath) var navigation_path : NodePath;
export(NodePath) var upper_corner_path : NodePath;
export(NodePath) var lower_corner_path : NodePath;
export(NodePath) var left_exit_path : NodePath;
export(NodePath) var right_exit_path : NodePath;

export(NodePath) var husband_stand_path : NodePath;
export(NodePath) var parts_manager_path : NodePath;

export(int) var money : int = 0;
export(int) var retirement_cost : int = 200;

var _navigation : Navigation2D;
var _store_size : Array;
var _left_exit : Vector2;
var _right_exit : Vector2;
var _husband_stand_manager : Node2D;
var _parts_manager : Node2D;

func _ready():
	_navigation = get_node(navigation_path);
	_store_size = [get_node(upper_corner_path).position, get_node(lower_corner_path).position];
	_left_exit = get_node(left_exit_path).position;
	_right_exit = get_node(right_exit_path).position;
	
	_husband_stand_manager = get_node(husband_stand_path);
	_parts_manager = get_node(parts_manager_path);
	
	randomize() # randomizes seed

func money_income(income : int):
	money += income;
	_husband_stand_manager.update_money(money);
	self.emit_signal("money_changed", money);

func money_expense(expense : int):
	money -= expense;
	_husband_stand_manager.update_money(money);
	self.emit_signal("money_changed", money);
	
func set_holding_husband(husband : Husband):
	_husband_stand_manager.set_husband(husband, money);
	if husband != null and husband.production_price > money:
		emit_signal("cant_produce");

func buy_random_part():
	if _parts_manager.is_sold_out:
		emit_signal("no_parts_to_buy");
	elif money < _parts_manager.unlock_cost:
		emit_signal("cant_buy_part");
	else:
		var new_part = _parts_manager.buy_random_part();
		money -= _parts_manager.unlock_cost;
		emit_signal("money_changed", money);
		emit_signal("unlocked_part", new_part);

func retire():
	if money < retirement_cost:
		emit_signal("cant_retire");
	else:
		money -= retirement_cost;
		emit_signal("money_changed", money);
		emit_signal("retired");

func get_store_path(curr_position : Vector2) -> PoolVector2Array:
	var random_point : Vector2 = Vector2(
			rand_range(_store_size[0].x, _store_size[1].x),
			rand_range(_store_size[0].y, _store_size[1].y)
	);
	var path = _navigation.get_simple_path(curr_position, random_point);
	path.remove(0); # 0 is curr_position
	return path;

func get_exit_path(curr_position : Vector2) -> PoolVector2Array:
	var random_point : Vector2 = get_entry_point();
	var path = _navigation.get_simple_path(curr_position, random_point);
	path.remove(0); # 0 is curr_position
	return path;

func get_entry_point() -> Vector2:
	return _left_exit if rand_range(0, 1.0) > 0.5 else _right_exit;
