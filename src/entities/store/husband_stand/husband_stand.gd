extends Area2D
class_name HusbandStand


signal stand_clicked(stand)
signal husband_bought(husband)


export(PackedScene) var husband_particles : PackedScene;
var husband_set : Husband setget set_husband;


func set_husband(value : Husband):
	husband_set = value;
	$Husband.visible = true if value != null else false;

func buy_husband(client):
	emit_signal("husband_bought", husband_set, client);
	# Particles!
	var particles = husband_particles.instance();
	self.call_deferred("add_child", particles);
	
	self.husband_set.call_deferred("free")
	self.husband_set = null;

func _on_input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton \
			and event.button_index == 1 \
			and event.pressed:
		emit_signal("stand_clicked", self)
