extends Particles2D

export(float) var particle_lifetime : float = 1;

func _ready():
	#print("Particles!");
	self.lifetime = particle_lifetime;
	$timer.wait_time = particle_lifetime;
	
	self.emitting = true;
	$timer.start();

func _on_timer_timeout():
	self.call_deferred("free");
