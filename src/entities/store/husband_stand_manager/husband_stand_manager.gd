extends Node2D

signal husband_built(cost);
signal husband_sold(price);
signal purchase_log(text);

var _holding_husband : Husband;
var _can_build : bool = false;

func _ready():
	for child in self.get_children():
		if child is HusbandStand:
			child.connect("husband_bought", self, "_husband_sold");
			child.connect("stand_clicked", self, "_stand_clicked");


func set_husband(husband : Husband, current_money : int):
	_holding_husband = husband;
	if husband != null and current_money >= husband.production_price:
		_can_build = true;

func update_money(money : int):
	if _holding_husband == null:
		return;
	
	if _holding_husband.production_price > money:
		_can_build = false;
	else:
		_can_build = true;

func _husband_sold(husband : Husband, client : Client):
	var log_text = client.client_name + " bought " + husband.husband_name + \
			 " for " + str(husband.sell_price);
	
	# Debug output
	#print(client.client_name + " bought " + husband.husband_name + " for " 
	#		+ str(husband.sell_price));
	# In game log
	self.emit_signal("purchase_log", log_text);
	self.emit_signal("husband_sold", husband.sell_price);

func _stand_clicked(stand : HusbandStand):
	if !(_can_build):
		return;
	
	if _holding_husband != null and stand.husband_set == null:
		stand.husband_set = _holding_husband.clone();
		self.emit_signal("husband_built", _holding_husband.production_price);
