extends Node2D

signal set_unlock_cost(cost);

export(Array, PackedScene) var initial_parts : Array = [];
export(Array, PackedScene) var parts_to_buy : Array = [];

var is_sold_out : bool = false;
export(int) var unlock_cost : int = 20;

var _parts_bought : int = 0;

func _ready():
	self.emit_signal("set_unlock_cost", unlock_cost);
	
	for husband_part in initial_parts:
		GlobalEnums.add_part(husband_part.instance());
	
	if parts_to_buy.size() == 0:
		is_sold_out = true;

func buy_random_part():
		var index = randi()%parts_to_buy.size();
		var husband_part = parts_to_buy.pop_at(index).instance();
		GlobalEnums.add_part(husband_part);
		_parts_bought += 1;
		
		if _parts_bought % 2 == 0:
			unlock_cost += 5;
			self.emit_signal("set_unlock_cost", unlock_cost);
		
		if parts_to_buy.size() == 0:
			is_sold_out = true;
		
		return husband_part;
