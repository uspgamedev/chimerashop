extends Node
class_name HusbandPart

enum HUSBAND_PART {HEAD, BODY, EXTRA}
enum HUSBAND_ATTR {STRENGTH, CHARM, BEAUTY}

export(String) var part_name : String = "Name";
export(int) var price : int = 0; # production price
export(HUSBAND_PART) var part_type : int = HUSBAND_PART.HEAD;
export(Dictionary) var values : Dictionary = {
	HUSBAND_ATTR.STRENGTH : 0,
	HUSBAND_ATTR.CHARM : 0,
	HUSBAND_ATTR.BEAUTY : 0,
};
export(StreamTexture) var part_image;

func get_strength() -> int:
	return values[HUSBAND_ATTR.STRENGTH];

func set_strength(strength : int) -> void:
	assert(typeof(strength) == TYPE_INT);
	values[HUSBAND_ATTR.STRENGTH] = strength;

func get_charm() -> int:
	return values[HUSBAND_ATTR.CHARM];

func set_charm(charm : int) -> void:
	assert(typeof(charm) == TYPE_INT);
	values[HUSBAND_ATTR.STRENGTH] = charm;

func get_beauty() -> int:
	return values[HUSBAND_ATTR.BEAUTY];

func set_beauty(beauty : int) -> void:
	assert(typeof(beauty) == TYPE_INT);
	values[HUSBAND_ATTR.STRENGTH] = beauty;
