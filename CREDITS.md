## Made by
- Matheus Lima Cunha
- Rodrigo Volpe Battistin

## Assets used
- Tileset: 1-Bit Pack, by Kenney (www.kenney.nl),
License: CC0
 (http://creativecommons.org/publicdomain/zero/1.0/)

- Music: Centaur Forest, by Kaptin_Random
(https://freesound.org/people/Kaptin_Random/),
License: CC0

- First names: first-names.txt, by Dominic Tarr
 (https://github.com/dominictarr),
[MIT License](https://github.com/dominictarr/random-name/blob/master/LICENSE)

- Animal names: animals.txt, by skjorrface
(https://github.com/skjorrface),
[MIT License](https://github.com/skjorrface/animals.txt/blob/master/LICENSE)

- Font: CatShop, by Peter Wiegel,
SIL Open Font License
